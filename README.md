# Essais de prototype SDAP en parallèle (MPI).

## 2 repertoires 
* numlab-sw  : 
  modèles reduit (modèle 2D eau peu profonde) de test de la librairie toy_sdap
* toy-sdap   : 
  prototype (module fortan, => libtoy_sdap.a)


## Prérequis
* compillos fortran
* mpi
* parallèle netcdf : http://cucis.ece.northwestern.edu/projects/PnetCDF/download.html

### Sur nuwa
* module load gnu/4.8.5 /home/sila/modules/libraries/vendor/gnu-4.8.5/gnu_ompi/1.10.6 gnu-4.8.5/gnu_ompi/1.10.6/pnetcdf

1. Compilation de toy-sdap
cd toy-sdap
le Makefile est à jour pour nuwa et les module indiqués ci-dessus
make => création de libtoy_sdap.a et toy_sdap.mod

2. Compilation du toy-model
../numlab-sw/
le Makefile est à jour pour nuwa

### Infos
Consulter et amender le WIKI :

https://gitlab.com/nguc/SDAP-mpi/wikis/SDAP-mpi
