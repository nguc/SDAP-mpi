close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATLAB Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pos_w = 0.025;   % To set position of windows
szx_w = 0.25;    % To set size of windows
szy_w = 0.5;
ifig  = 0;      % Ifig is added to the Figure number.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reads Configuration ID/grid file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rdid=numlab_readid('OUTPUT/numlab_id.dat');

dt=str2num(rdid{1});
nt=str2num(rdid{2});
dx=str2num(rdid{3});
nx=str2num(rdid{4});
dy=str2num(rdid{5});
ny=str2num(rdid{6});
ifl_conf=rdid{7};
ifl_time=rdid{8};
ifl_adv =rdid{9};
ifl_diff=rdid{10};
ifl_cor =rdid{11};
ifl_div =rdid{12};
ifl_grad=rdid{13};

load('OUTPUT/numlab_grid.dat')
load('OUTPUT/numlab_bathy.dat')

x=transpose(reshape(numlab_grid(:,1),ny,nx)/1000);
y=transpose(reshape(numlab_grid(:,2),ny,nx)/1000);
bathy=transpose(reshape(numlab_bathy,ny,nx));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ASCII output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(['  '])
disp(['----------------------------------'])
disp(['   ifl_conf  = ' ifl_conf])
disp(['   ifl_time  = ' ifl_time])
disp(['   ifl_adv   = ' ifl_adv ])
disp(['   ifl_diff  = ' ifl_diff])
disp(['   ifl_div   = ' ifl_div])
disp(['   ifl_cor   = ' ifl_cor])
disp(['   ifl_grad  = ' ifl_grad])
disp(['          dt = ' num2str(dt)])
disp(['          nt = ' num2str(nt)])
disp(['          dx = ' num2str(dx)])
disp(['          nx = ' num2str(nx)])
disp(['          dy = ' num2str(dy)])
disp(['          ny = ' num2str(ny)])
disp(['  '])
disp(['----------------------------------'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reads Configuration ASCII outputs (phi & psi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
phic0=load('OUTPUT/numlab_phi_c.dat');
phih0=load('OUTPUT/numlab_phi_h.dat');
phiu0=load('OUTPUT/numlab_phi_hu.dat');
phiv0=load('OUTPUT/numlab_phi_hv.dat');
psi0 =load('OUTPUT/numlab_psi.dat');
t    =load('OUTPUT/numlab_time.dat');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Resahape phi, psi, x & t ==> Matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
phih=permute(reshape(phih0(:,1),ny,nx,nt),[2 1 3]);
phic=permute(reshape(phic0(:,1),ny,nx,nt),[2 1 3]);
phiu=permute(reshape(phiu0(:,1),ny,nx,nt),[2 1 3]);
phiv=permute(reshape(phiv0(:,1),ny,nx,nt),[2 1 3]);
psi =permute(reshape(psi0 (:,1),ny,nx,nt),[2 1 3]);

if 1==1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure (1): Movies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
h1=figure(1);subplot(1,1,1);hold off
set(h1,'Name','Movies')
set(h1,'NumberTitle','off')
set(h1,'units','normalized')
set(h1,'Position',[pos_w pos_w szy_w szx_w])

for it=1:nt % Time-loop:
    %phiht=phih(:,:,it)-1000;
    %mean(mean(phiht(:,:)))
    figure(1);subplot(1,1,1);hold off     
    if ~strcmpi(ifl_conf,'SPIKESW')  & ~strcmpi(ifl_conf,'WAVESW')  ...
                                     & ~strcmpi(ifl_conf,'WAVE2SW') ...
                                     & ~strcmpi(ifl_conf,'SHEARSW')
           
       hh=mean(mean(phih(:,:,1))); 
       subplot(2,3,1)
       hold off
       contourf(x(:,:),y(:,:),phih(:,:,it)-hh)
       hold on
       quiver(x(:,:),y(:,:),phiu(:,:,it),phiv(:,:,it),'k')
       xlabel('x (km)')
       ylabel('y (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       %colorbar('hor','location','SouthOutside')
       title('\Phi_h(t)')
       colorbar('hor','location','SouthOutside')
       
       subplot(2,3,4)
       surfc(x(:,:),y(:,:),phih(:,:,it)-hh)
       xlabel('x (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'ZLim',[min(min(min(phih(:,:,:)-hh)))*1.2 max(max(max(phih(:,:,:)-hh)))*1.2])
       title('\Phi_h(t)')
       
    elseif strcmpi(ifl_conf,'SPIKESW')        
       hold off       
       subplot(2,3,1)
       hold off
       contourf(x(:,:),y(:,:),psi(:,:,it))
       xlabel('x (km)')
       ylabel('y (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       %set(gca,'ZLim',[0 1.1])
       %colorbar('hor','location','SouthOutside')
       colorbar('hor','location','SouthOutside')
       title('\Psi(t)')
       
       subplot(2,3,4)
       surfc(x(:,:),y(:,:),psi(:,:,it))
       xlabel('x (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       set(gca,'ZLim',[min(min(psi(:,:,1)))*0.9 max(max(psi(:,:,1)))*1.1])
       title('\Psi(t)')
       hold off       
       
       subplot(2,3,2)
       hold off
       contourf(x(:,:),y(:,:),phic(:,:,it))
       xlabel('x (km)')
       ylabel('y (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       %set(gca,'ZLim',[0 1.1])
       %colorbar('hor','location','SouthOutside')
       colorbar('hor','location','SouthOutside')
       title('\Phi(t)')
       
       'coucou'
       subplot(2,3,5)
       surfc(x(:,:),y(:,:),phic(:,:,it))
       xlabel('x (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       set(gca,'ZLim',[min(min(phic(:,:,1)))*0.9 max(max(phic(:,:,1)))*1.1])
       title('\Phi(t)')
       
    elseif strcmpi(ifl_conf,'WAVESW')      
       hold off       
       subplot(2,3,1)
       hold off
       contourf(x(:,:),y(:,:),psi(:,:,it))
       xlabel('x (km)')
       ylabel('y (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       %set(gca,'ZLim',[0 1.1])
       %colorbar('hor','location','SouthOutside')
       colorbar('hor','location','SouthOutside')
       title('\Psi(t)')
       
       subplot(2,3,4)
       surfc(x(:,:),y(:,:),psi(:,:,it))
       xlabel('x (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       set(gca,'ZLim',[min(min(psi(:,:,1)))*0.9 max(max(psi(:,:,1)))*1.1])
       title('\Psi(t)')
       hold off       
       
       subplot(2,3,2)
       hold off
       contourf(x(:,:),y(:,:),phic(:,:,it))
       xlabel('x (km)')
       ylabel('y (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       %set(gca,'ZLim',[0 1.1])
       %colorbar('hor','location','SouthOutside')
       colorbar('hor','location','SouthOutside')
       title('\Phi(t)')
       
       subplot(2,3,5)
       surfc(x(:,:),y(:,:),phic(:,:,it))
       xlabel('x (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       set(gca,'ZLim',[min(min(phic(:,:,1)))*0.9 max(max(phic(:,:,1)))*1.1])
       title('\Phi(t)')
       
    elseif strcmpi(ifl_conf,'WAVE2SW')       
       hold off       
       subplot(2,3,1)
       hold off
       contourf(x(:,:),y(:,:),psi(:,:,it))
       xlabel('x (km)')
       ylabel('y (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       %set(gca,'ZLim',[0 1.1])
       %colorbar('hor','location','SouthOutside')
       colorbar('hor','location','SouthOutside')
       title('\Psi(t)')
       
       subplot(2,3,4)
       surfc(x(:,:),y(:,:),psi(:,:,it))
       xlabel('x (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       set(gca,'ZLim',[min(min(psi(:,:,1)))*0.9 max(max(psi(:,:,1)))*1.1])
       title('\Psi(t)')
       hold off       
       
       subplot(2,3,2)
       hold off
       contourf(x(:,:),y(:,:),phih(:,:,it))
       xlabel('x (km)')
       ylabel('y (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       %set(gca,'ZLim',[0 1.1])
       %colorbar('hor','location','SouthOutside')
       colorbar('hor','location','SouthOutside')
       title('\Phi(t)')
       
       subplot(2,3,5)
       surfc(x(:,:),y(:,:),phih(:,:,it))
       xlabel('x (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       set(gca,'ZLim',[min(min(phih(:,:,1)))*0.9 max(max(phih(:,:,1)))*1.1])
       title('\Phi(t)')
       
    elseif strcmpi(ifl_conf,'SHEARSW')  
       subplot(2,3,1)
       hold off
       contourf(x(:,:),y(:,:),phiu(:,:,it))
       hold on
       quiver(x(:,:),y(:,:),phiu(:,:,it),phiv(:,:,it),'k')
       xlabel('x (km)')
       ylabel('y (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'YLim',[min(min(y)) max(max(y))])
       %colorbar('hor','location','SouthOutside')
       title('\Phi_u(t)')
       colorbar('hor','location','SouthOutside')
       
       subplot(2,3,4)
       surfc(x(:,:),y(:,:),phiu(:,:,it))
       xlabel('x (km)')
       set(gca,'XLim',[min(min(x)) max(max(x))])
       set(gca,'ZLim',[min(min(phiu(:,:,1)))*0.9 max(max(phiu(:,:,1)))*1.1])
       title('\Phi_u(t)')
    end 
    
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Plots passive Tracer:
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   if sum(sum(sum(phic(:,:,:)))) ~=0 
      subplot(2,3,3)
      hold off
      contourf(x(:,:),y(:,:),phic(:,:,it))
      hold on
      quiver(x(:,:),y(:,:),phiu(:,:,it),phiv(:,:,it),'k')
      xlabel('x (km)')
      set(gca,'XLim',[min(min(x)) max(max(x))])
      set(gca,'YLim',[min(min(y)) max(max(y))])
      %set(gca,'ZLim',[0 1.1])
      %colorbar('hor','location','SouthOutside')
      title('\Phi_c(t)')
      colorbar('hor','location','SouthOutside')
       
      subplot(2,3,6)
      hold off
      surfc(x(:,:),y(:,:),phic(:,:,it))
      xlabel('x (km)')
      ylabel('y (km)')
      set(gca,'XLim',[min(min(x)) max(max(x))])
      set(gca,'YLim',[min(min(y)) max(max(y))])
      if phic ~=0
         set(gca,'ZLim',[min(min(min(phic(:,:,:))))*0.9 max(max(max(phic(:,:,:))))*1.1])
      end
      title('\Phi_c(t)')
   end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plots divergence and vorticity:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h2=figure(2);subplot(1,1,1);hold off
set(h2,'Name','Movies (2)')
set(h2,'NumberTitle','off')
set(h2,'units','normalized')
set(h2,'Position',[2*pos_w 2*pos_w szy_w szx_w])

if strcmpi(ifl_conf,'WAVE2SW') | strcmpi(ifl_conf,'GEO1SW') | ...
   strcmpi(ifl_conf,'SPLASHSW')
clear div
clear vort

for it=2:nt % Time-loop:
   figure(2);subplot(1,1,1);hold off 
   for is = 1 : nx-1
   for js = 1 : ny-1
       im1 = mod(is-1-1,nx)+1;
       ip1 = mod(is+1-1,nx)+1;
       jm1 = mod(js-1-1,ny)+1;
       jp1 = mod(js+1-1,ny)+1;
    
       div (is,js) = ( phiu(ip1,js ,it) - phiu(is ,js ,it) ) / dx  ...
                   + ( phiv(is ,jp1,it) - phiv(is ,js ,it) ) / dy;
       vort(is,js) = ( phiv(is ,js ,it) - phiv(im1,js ,it) ) / dx  ...
                   - ( phiu(is ,js ,it) - phiu(is ,jm1,it) ) / dy;
   end
   end

   subplot(2,2,1)
   hold off
   contourf(dx/2e3 + x(1:end-1,1:end-1),dy/2e3 + y(1:end-1,1:end-1),div(:,:))
   hold on
   quiver(x(:,:),y(:,:),phiu(:,:,it),phiv(:,:,it),'k')
   xlabel('x (km)')
   set(gca,'XLim',[min(min(x)) max(max(x))])
   set(gca,'YLim',[min(min(y)) max(max(y))])
   %set(gca,'ZLim',[0 1.1])
   %colorbar('hor','location','SouthOutside')
   title('Divergence')
   colorbar('hor','location','SouthOutside')

   subplot(2,2,3)
   hold off
   surfc(dx/2e3 + x(1:end-1,1:end-1),dy/2e3 + y(1:end-1,1:end-1),div(:,:))
   xlabel('x (km)')
   ylabel('y (km)')
   set(gca,'XLim',[min(min(x)) max(max(x))])
   set(gca,'YLim',[min(min(y)) max(max(y))])
   if div ~= 0
      set(gca,'ZLim',[min(min(div(:,:)))*0.9 max(max(div(:,:)))*1.1])
   end
   %set(gca,'ZLim',[0 1.1])
   %colorbar('hor','location','SouthOutside')
   title('Divergence')
       
   subplot(2,2,2)
   hold off
   contourf(dx/2e3 + x(1:end-1,1:end-1),dy/2e3 + y(1:end-1,1:end-1),vort(:,:))
   hold on
   quiver(x(:,:)',y(:,:)',phiu(:,:,it)',phiv(:,:,it)','k')
   xlabel('x (km)')
   set(gca,'XLim',[min(min(x)) max(max(x))])
   set(gca,'YLim',[min(min(y)) max(max(y))])
   %set(gca,'ZLim',[0 1.1])
   %colorbar('hor','location','SouthOutside')
   title('Vorticity')
   colorbar('hor','location','SouthOutside')

   subplot(2,2,4)
   hold off
   surfc(dx/2e3 + x(1:end-1,1:end-1),dy/2e3 + y(1:end-1,1:end-1),vort(:,:))
   xlabel('x (km)')
   ylabel('y (km)')
   set(gca,'XLim',[min(min(x)) max(max(x))])
   set(gca,'YLim',[min(min(y)) max(max(y))])
   if vort ~= 0
      set(gca,'ZLim',[min(min(vort(:,:)))*0.9 max(max(vort(:,:)))*1.1])
   end
   title('Vorticity')
end
end

if strcmpi(ifl_conf,'DAMBREAK') 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure (3): 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
h3=figure(3);subplot(1,1,1);hold off
set(h3,'Name','Movies (3)')
set(h3,'NumberTitle','off')
set(h3,'units','normalized')
set(h3,'Position',[2*pos_w 2*pos_w szy_w szx_w])

for it = 1:nt
    figure (3);subplot(1,1,1);hold off 
    subplot(2,1,1)
    hold off
    plot(x(:,1),phih(:,1,it),'r')
    hold on
    plot(x(:,1),psi(:,1,it),'b')
    set(gca,'XLim',[min(min(x)) max(max(x))])
    if strcmpi(ifl_conf,'DAMBREAK')
        set(gca,'YLim',[0 1.1])
    end
    ylabel('\Psi_h & \Phi_h')
    legend('\Psi_h','\Phi_h')
    
    subplot(2,1,2)
    hold off
    plot(x(:,1),phiu(:,1,it),'r')
    set(gca,'XLim',[min(min(x)) max(max(x))])
    if strcmpi(ifl_conf,'DAMBREAK')
        set(gca,'YLim',[-1.5 1.5])
    end
    xlabel('x (km)')
    ylabel('\Phi_u')
    legend('\Phi_u')
    
    title('Vertical Section')
end
end

end

if strcmpi(ifl_conf,'SPLASHSW') 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure (3): 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
h4=figure(4);subplot(1,1,1);hold off
set(h4,'Name','Conservation')
set(h4,'NumberTitle','off')
set(h4,'units','normalized')
set(h4,'Position',[3*pos_w 3*pos_w szy_w szx_w])

cref=mean(mean(phic(:,:,1)));
for it = 1:nt
    c(it)=mean(mean(phic(:,:,it)))/cref-1;
end

plot(t,c*100)
xlabel('time (s)')
ylabel('Err(C) (%)')
title('Tracer Conservation')
end



    
