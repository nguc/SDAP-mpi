%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%         NUMLAB: Numerical Laboratory
%
%     Plots, Diagnostics & Post-treatments 
%
% Reads ID-ASCII file
%
%                              F. Auclair
%                              LA/OMP/UPS
%                              CROCO Project
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function rdfile = numlab_readid(filename)

% Clear variables & various initialization:
clear rdfile
tline=' ';
il=0;

% Open file to be read:
fid = fopen(filename);

% while loop to read file:
while ischar(tline)
    
    % Space & empty string are not stored:
    if ~strcmpi(tline,' ') & ~strcmpi(tline,'')
       il=il+1;
       rdfile{il}=tline;
    end
    
    % Get new line:
    tline = fgetl(fid);
    ic_ok=0;
    for ic = 1:length(tline)
        if ~strcmpi(tline(ic),' ')
            ic_ok = ic_ok + 1;
            tline(ic_ok)=tline(ic);
        end
    end
    tline=tline(1:ic_ok);
end

% Close file:
fclose(fid);
